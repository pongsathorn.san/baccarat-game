<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Game</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

        <link rel="stylesheet" href="game.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </head>
    <body>

        <?php
        $row = 6;
        $col = 12;

        $players[] = [
            'id' => 0,
            'title' => 'PLAYER',
            'value' => 'btn-primary',
            'color' => 'mediumblue',
        ];
        $players[] = [
            'id' => 1,
            'title' => 'TIE',
            'value' => 'btn-success',
            'color' => 'limegreen',
        ];
        $players[] = [
            'id' => 2,
            'title' => 'BANKER',
            'value' => 'btn-danger',
            'color' => 'red'
        ];
        ?>
        <div class="container">
            <h2>Game</h2>

            <div class="row">
                <div class="col-sm-8 col-md-8 col-gl-8">
                    <div class="board-wrap">
                        <?php for ($i = 0; $i <= $row; $i++): ?>
                            <div class="board-row">
                                <?php for ($j = 0; $j <= $col; $j++): ?>
                                    <div class="board-col" id="cell-<?= $i ?>-<?= $j ?>">                  
                                        <div class="selected"></div>
                                    </div>
                                <?php endfor; ?>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <!------------------------------------------------>
                    <br/>
                    <div class="alert res-alert text-center">
                        รอสูตร
                    </div>
                    <p class="text-center">
                        <?php
                        $toJs = [];
                        foreach ($players as $key => $player):
                            $toJs[] = $player['value'];
                            ?>

                            <button class="btn play <?= $player['value'] ?>" value="<?= $key ?>"><?= $player['title'] ?></button>
                            <?php
                        endforeach;
                        ?>
                    </p> 
                    <br/>
                    <p class="text-center">
                        <button class="btn btn-warning new_game">New Game</button>
                        <button class="btn btn-info prev_game" disabled="disabled">Prev Game</button>
                    </p> 

                </div>
                <div class="col-sm-4 col-md-4 col-gl-4">
                    <table class="table table-bordered score">
                        <thead>
                            <tr>
                                <th>
                                    ครั้งที่
                                </th>
                                <th>
                                    ไม้ที่
                                </th>
                                <th>
                                    ผล
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <table class="table table-bordered sammary">
                        <thead>
                            <tr>
                                <th>
                                    รวม
                                </th>
                                <th>
                                    ชนะ
                                </th>
                                <th>
                                    แพ้
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--https://divtable.com/ 
        http://rich55.net/index.php-->

        <script type="text/javascript">
            var items = <?= json_encode($players) ?>;
            var ranItems = <?= json_encode($players) ?>;
            console.log(items);

            for (var key in ranItems) {
                if (key == 1) {
                    ranItems.splice(key, 1);
                }
            }

            //delete ranItems[1];
            console.log(ranItems);
        </script>
        <script src="game.js"></script>
    </body>
</html>