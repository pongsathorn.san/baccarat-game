<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<?php
$row = 6;
$col = 12;
?>
<div class="board-wrap">
    <?php for ($i = 0; $i <= $row; $i++): ?>
        <div class="board-row">
            <?php for ($j = 0; $j <= $col; $j++): ?>
                <div  class="board-col" id="cell-<?= $i ?>-<?= $j ?>">                  
                    <div class="selected"></div>
                </div>
            <?php endfor; ?>
        </div>
    <?php endfor; ?>
</div>
<hr/>
<p>
    <button class="btn btn-primary play" value="btn-primary">Player 1</button>
    <button class="btn btn-success play" value="btn-success">Player 2</button>
    <button class="btn btn-danger play" value="btn-danger" >Player 3</button>
</p>
<p>
    <button class="btn btn-default new_game">New Game</button>
</p>


<style>
    .board-wrap{
        background: #000;
        border: 1px solid #000;
        display: table;
        padding:3px;

    }
    .board-wrap::before{
        /*        clear: both;*/
    }
    .board-wrap .board-row{
        /*display: inline-block;*/
        display: table-row;
        /*        width: 100%;*/
    }
    .board-wrap .board-col{
        display: inline-block;
        width: 40px;
        height: 40px;
        border: 1px solid #000;
        border-radius: 3px;
        margin: 0px;
        background: #fff;
        padding:3px;
    }
    .board-wrap .board-col:hover{
        background: #aaa;
    }
    .board-col .selected{
        width: 100%;
        height: 100%;
        border-radius: 25px;
        box-shadow: 1px 1px 5px #888;
        display:none;
    }

</style>
<script type="text/javascript">
    var row = 0;
    var col = 0;
    var rowMax = 6;
    var colMax = 12;
    var end_game = true;
    $(function () {

        beginGame();
        $(".new_game").bind('click', function () {
            end_game = false;
            startPlayer();
        });

    });

    function startPlayer() {
        $(".play").removeClass('disabled');
        $(".play").bind('click', function () {
            if (!end_game) {
                var valSelect = $(this).val();
                $('div#cell-' + row + '-' + col + ' .selected').addClass(valSelect).show();
                getKey();
            } else {
                alert('End Game');
                beginGame();
            }
        });
    }

    function beginGame() {
        $(".play").unbind('click');
        $(".play").addClass('disabled');
        $('div#board-col .selected');
    }
    function endGame() {
        $(".play").unbind('click');
        $(".play").addClass('disabled');
        //$('div#cell-' + row + '-' + col + ' .selected');
    }

    function getKey() {
        console.log('Before row:' + row + "col:" + col);
        if (row < rowMax) {
            ++row;
        } else {
            if (col < colMax) {
                ++col;
            } else {
                end_game = true;
                beginGame();
            }
            row = 0;
        }
        console.log('After row:' + row + "col:" + col);
    }
</script>

<!--https://divtable.com/ 
http://rich55.net/index.php-->
<script src="game.js"></script>